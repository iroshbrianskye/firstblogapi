'use strict';
module.exports = {
  ERROR: {
    status: 0,
    message: 'failed',
  },
  SUCCESS: {
    status: 1,
    message: 'success',
  },
  unique(arr) {
    return arr.filter(function(item, index, arr) {
      return arr.indexOf(item) === index;
    });
  },
};
